// Load dependencies.
const fs = require('fs');
const path = require('path');
const { exec } = require("child_process");
const moment = require('moment');
const md = require('md-directory');
const marked = require('marked');
const sass = require('sass');

// Process the light and dark CSS themes.
const lightCSS = sass.renderSync({ file: path.join(__dirname, 'css/index.scss') });

// If the directory does not exist..
if(!fs.existsSync(path.join(__dirname, 'public/css/')))
{
	// .. create it.
	fs.mkdirSync(path.join(__dirname, 'public/css/'), { recursive: true });
}

// Write the light and dark CSS themes to disk.
fs.writeFileSync(path.join(__dirname, 'public/css/style.css'), lightCSS.css);

//
//const sourceRepository = 'https://gitlab.com/bitcoinunlimited/BitcoinCashSpecification';
const sourceRepository = 'https://github.com/softwareverde/bitcoin-cash-specification';
const sourceFolder = path.basename(sourceRepository);

// Clonse the git repository.
exec(`git clone ${sourceRepository}`);

// Recursively parse all the markdown content.
const content = md.parseDirSync(`./${sourceFolder}/`, { dirnames: true, md: marked });

// Load the template.
const template = fs.readFileSync(path.join(__dirname, 'template.html')).toString();

// Initialize an empty list of pages.
const pages = {};

// For each parsed markdown file..
for(page in content)
{
	// Insert the content into a copy of the template.
	pages[page] = template.replace('<!-- content goes here -->', content[page].content);

	// Insert the current date into the footer.
	pages[page] = pages[page].replace('<!-- timestamp goes here -->', moment.utc().format('MMMM Do, YYYY'));

	// Insert the current source repository into the footer.
	pages[page] = pages[page].replace('<!-- source repo goes here -->', sourceRepository);

	// Define patterns to detect internal and external links.
	const internalLinksPattern = /<a href=["'](?<link>\/[^"']+)["']/gi;
	const externalLinksPattern = /<a href="(https?:)?\/\//gi

	// Set external reference for external links. (rel=external)
	pages[page] = pages[page].replace(externalLinksPattern, '<a rel="external" target="_blank" href="$1//');

	// Set up an empty list of internal links so we can add file extensions.
	const replacements = new Set();

	// Match all internal links.
	const matches = pages[page].matchAll(internalLinksPattern);

	// For each internal link..
	for(const match of matches)
	{
		// Assume link does not have trailing slash.
		let sourcePath = `/${sourceFolder}${match.groups.link}`;

		// Remove page references from the source path.
		sourcePath = sourcePath.split('#')[0];

		// Remove trailing slash if necessary.
		if(match.groups.link.endsWith('/'))
		{
			sourcePath = `/${sourceFolder}${match.groups.link}`.slice(0, -1);
		}

		// Remove manually placed '.md' if necessary.
		if(match.groups.link.endsWith('.md'))
		{
			sourcePath = `/${sourceFolder}${match.groups.link}`.slice(0, -3);
		}

		// Check if an internal file exist for this link, and if so..
		if(fs.existsSync(path.join(__dirname, `/${sourcePath}.md`)))
		{
			// Mark this as an file to add extension to.
			replacements.add(match.groups.link);
		}
		else
		{
			// TODO: Remove this when there is no missing or broken links in the source data.
			//console.log(`Missing link in: ${page}: ${sourcePath}`);
			//console.log(pages[page]);
			//process.exit();
		}
	}

	// For each file that we should add a file extension to..
	for(const replacement of replacements)
	{
		// Update internal link to add file extension.
		pages[page] = pages[page].replaceAll(`${replacement}'`, `${replacement}.html'`);
		pages[page] = pages[page].replaceAll(`${replacement}"`, `${replacement}.html"`);
		pages[page] = pages[page].replaceAll(`${replacement}#`, `${replacement}.html#`);
	}

	// Derive the directory for where we want to store the page.
	const directory = path.dirname(path.join(__dirname, `public/${page}.html`));

	// If the directory does not exist..
	if(!fs.existsSync(directory))
	{
		// .. create it.
		fs.mkdirSync(directory, { recursive: true });
	}

	// Store the page to disk.
	fs.writeFileSync(path.join(__dirname, `public/${page.replace(/\.md$/g, '')}.html`), pages[page]);
}
